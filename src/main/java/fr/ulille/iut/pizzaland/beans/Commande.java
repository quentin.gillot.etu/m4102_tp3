package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

public class Commande {
	private UUID id;
	private String name;
	private List <Pizza> pizzas;
	public Commande(UUID id, String name, List<Pizza> pizzas) {
		super();
		this.id = id;
		this.name = name;
		this.pizzas = pizzas;
	}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Pizza> getPizzas() {
		return pizzas;
	}
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	
}

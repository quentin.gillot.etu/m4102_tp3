package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.beans.Commande;

public interface CommandeDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (id VARCHAR(128) PRIMARY KEY, username VARCHAR UNIQUE NOT NULL, firstname VARCHAR UNIQUE NOT NULL)")
    void createTable();

    @SqlUpdate("DROP TABLE IF EXISTS commandes")
    void dropTable();

    @SqlUpdate("INSERT INTO commandes (id, name) VALUES (:id, :name)")
    void insert(@BindBean Commande command);

    @SqlUpdate("DELETE FROM commands WHERE id = :id")
    void remove(@Bind("id") UUID id);

    @SqlQuery("SELECT * FROM commandes WHERE username = :username")
    @RegisterBeanMapper(Commande.class)
    Commande findByUsername(@Bind("username") String username);

    @SqlQuery("SELECT * FROM commandes")
    @RegisterBeanMapper(Commande.class)
    List<Commande> getAll();

    @SqlQuery("SELECT * FROM commandes WHERE id = :id")
    @RegisterBeanMapper(Commande.class)
    Commande findById(@Bind("id") UUID id);
}
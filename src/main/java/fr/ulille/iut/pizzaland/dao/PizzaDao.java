package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	

	/*
	 * CREATION ET DESTRUCTION DES TABLES
	 */
  @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
  void createPizzaTable();
  @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (PizzaId VARCHAR(128), IngredientId VARCHAR(128), PRIMARY KEY (PizzaId, IngredientId))")
  void createAssociationTable();
  
  @SqlUpdate("DROP TABLE IF EXISTS pizzas")
  void dropPizzaTable();
  @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
  void dropAssociationTable();
  
  @Transaction
  default void createTableAndIngredientAssociation() {
    createAssociationTable();
    createPizzaTable();
  }
  @Transaction
  default void dropTableAndIngredientAssociation() {
    dropAssociationTable();
    dropPizzaTable();
  }
  
  
  /*
   * INSERT
   */
  @SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
  void insertPizza(@BindBean Pizza pizza);
  @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (PizzaId, IngredientId) VALUES (:pizza.id, :ingredient.id)")
  void insertAssociation(@BindBean Pizza pizza, @BindBean Ingredient ingredient);
  
  default void insertPizzaInAssociation(Pizza p) {
	  //pizza dans table pizza
	  this.insertPizza(p);
	  //dans la table association --> l'id de la pizza est associé à plusieurs id ingrédients à la chaine 1/1 1/2 1/3 etc...
	  for(Ingredient i : p.getIngredients()) {
		  this.insertAssociation(p, i);
	  }
  }
  /*
   * REMOVE UNE PIZZA
   */
  @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
  void removePizza(@Bind("id") UUID id);
  
  
  
  /*
   * TROUVER PIZZAS
   */
  @SqlQuery("SELECT * FROM Pizzas")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll();
  
  @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
  @RegisterBeanMapper(Pizza.class)
  Pizza findByName(@Bind("name") String name);

  @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
  @RegisterBeanMapper(Pizza.class)
  Pizza findById(@Bind("id") UUID id);
  
  /*
  @SqlQuery("Select * from ingredients where id in(SELECT ingredientID FROM PizzaIngredientsAssociation WHERE pizzaID = :id)")
  @RegisterBeanMapper(Ingredient.class)
 List<Ingredient> findIngredientsById(@Bind("id") UUID id);
	
	default void insertTablePizzaAndIngredientAssociation(Pizza pizza) {
  	this.insert(pizza);
  	for(Ingredient ingr : pizza.getIngredients()) {
  		this.insertPizzaAndIngredientAssociation(pizza,ingr);
  	}
  }
	@SqlQuery("SELECT ingredientId FROM PizzaIngredientsAssociation where pizzaID=:idPizza")
  @RegisterBeanMapper(Pizza.class)
  List<UUID> getAllIngredientsID(@Bind("idPizza") UUID idPizza);
	
	@Transaction
  default List<Ingredient> getAllIngredients(List<UUID> idIngredients){
		IngredientDao ingredients = BDDFactory.buildDao(IngredientDao.class);
		List<Ingredient> ingredient=new ArrayList<>();
		for(UUID id: idIngredients) {
			ingredient.add(ingredients.findById(id));
		}
		return ingredient;
  }
	
	@Transaction
  default Pizza getTableAndIngredientAssociation(UUID id) {
    Pizza p=findById(id);
    List<UUID> ingredientsID=getAllIngredientsID(id);
    p.setIngredients(getAllIngredients(ingredientsID));
    return p;
  }*/
}
package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
   private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
    private PizzaDao dao;
    

    @Override
    protected Application configure() {
    	BDDFactory.setJdbiForTests();
        return new ApiV1();
    }

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
    // base de données
    // et les DAO

    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
    	dao = BDDFactory.buildDao(PizzaDao.class);
        dao.createTableAndIngredientAssociation();
    }

    @After
    public void tearEnvDown() throws Exception {
        dao.dropTableAndIngredientAssociation();
    }
    @Test
    public void testCreatePizza() {
    	
    	PizzaCreateDto PizzaCreateDto = new PizzaCreateDto();
    	PizzaCreateDto.setName("Chorizo");

        Response response = target("/pizzas").request().post(Entity.json(PizzaCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
        assertEquals(returnedEntity.getName(), PizzaCreateDto.getName());
    }
    
    
    @Test
    public void testGetEmptyList() {
        Response response = target("/pizzas").request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });

        assertEquals(0, pizzas.size());

    }
    
    @Test
    public void testGetExistingPizza() {
      Pizza pizza = new Pizza();
      pizza.setName("Chorizo");
      dao.insertPizza(pizza);

      Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

      assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

      Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
      assertEquals(pizza, result);
    }
    
    @Test
    public void testGetNotExistingPizza() {
      Response response = target("/pizzas").path("123tunexistespas").request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    
    
    
    @Test
    public void testDeleteExistingPizza() {
    	Pizza pizza = new Pizza();
    	pizza.setName("Chorizo");
	  dao.insertPizza(pizza);
	
	  Response response = target("/pizzas").path(pizza.getId().toString()).request().delete();
	
	  assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());
	
	  Pizza result = dao.findById(pizza.getId());
	  assertEquals(result, null);
   }

   @Test
   public void testDeleteNotExistingPizza() {
     Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }
   
   @Test
   public void testGetPizzaName() {
	   Pizza pizza = new Pizza();
     pizza.setName("Chorizo");
     dao.insertPizza(pizza);

     Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

     assertEquals("Chorizo", response.readEntity(String.class));
  }

  @Test
  public void testGetNotExistingPizzaName() {
    Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
  }

    @Test
    public void testCreateSamePizza() {
    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        dao.insertPizza(pizza);

        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreatePizzaWithoutName() {
        IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();

        Response response = target("/ingredients").request().post(Entity.json(ingredientCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }   
}
`
<html>
<table data-sourcepos="60:1-66:194" dir="auto">
<thead>
<tr data-sourcepos="60:1-60:197">
<th data-sourcepos="60:2-60:27" align="left">URI</th>
<th data-sourcepos="60:29-60:42" align="left">Opération</th>
<th data-sourcepos="60:44-60:105" align="left">MIME</th>
<th data-sourcepos="60:107-60:124" align="left">Requête</th>
<th data-sourcepos="60:126-60:196" align="left">Réponse</th>
</tr>
</thead>
<tbody>
<tr data-sourcepos="62:1-62:195">
<td data-sourcepos="62:2-62:27" align="left">/commandes</td>
<td data-sourcepos="62:29-62:41" align="left">GET</td>
<td data-sourcepos="62:43-62:104" align="left">&lt;-application/json<br>&lt;-application/xml</td>
<td data-sourcepos="62:106-62:122" align="left"></td>
<td data-sourcepos="62:124-62:194" align="left">liste des commandes (I2)</td>
</tr>
<tr data-sourcepos="63:1-63:195">
<td data-sourcepos="63:2-63:27" align="left">/commandes/{id}</td>
<td data-sourcepos="63:29-63:41" align="left">GET</td>
<td data-sourcepos="63:43-63:104" align="left">&lt;-application/json<br>&lt;-application/xml</td>
<td data-sourcepos="63:106-63:122" align="left"></td>
<td data-sourcepos="63:124-63:194" align="left">une commande (I2) ou 404</td>
</tr>
<tr data-sourcepos="64:1-64:195">
<td data-sourcepos="64:2-64:27" align="left">/commandes/{id}/name</td>
<td data-sourcepos="64:29-64:41" align="left">GET</td>
<td data-sourcepos="64:43-64:104" align="left">&lt;-text/plain</td>
<td data-sourcepos="64:106-64:122" align="left"></td>
<td data-sourcepos="64:124-64:194" align="left">le nom de la commande ou 404</td>
</tr>
<tr data-sourcepos="65:1-65:200">
<td data-sourcepos="65:2-65:27" align="left">/commandes</td>
<td data-sourcepos="65:29-65:41" align="left">POST</td>
<td data-sourcepos="65:43-65:104" align="left">&lt;-/-&gt;application/json<br>-&gt;application/x-www-form-urlencoded</td>
<td data-sourcepos="65:106-65:123" align="left">Commande (I1)</td>
<td data-sourcepos="65:125-65:199" align="left">Nouvelle commande (I2)<br>409 si la commande existe déjà (même nom)</td>
</tr>
<tr data-sourcepos="66:1-66:194">
<td data-sourcepos="66:2-66:27" align="left">/commandes/{id}</td>
<td data-sourcepos="66:29-66:41" align="left">DELETE</td>
<td data-sourcepos="66:43-66:104" align="left"></td>
<td data-sourcepos="66:106-66:122" align="left"></td>
<td data-sourcepos="66:124-66:193" align="left"></td>
</tr>
</tbody>
</table>`
</html>
